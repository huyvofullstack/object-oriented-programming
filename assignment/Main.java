import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        SinhVienCNTT a = new SinhVienCNTT(1, 1, 2, "301", 51403394, "Nguyen", "A");
        SinhVienCNTT b = new SinhVienCNTT(2, 2, 1, "301", 51403395, "Nguyen", "A");
        SinhVienCNTT c = new SinhVienCNTT(3, 1, 1, "301", 51403396, "Nguyen", "B");
        SinhVienCNTT d = new SinhVienCNTT(15, 2, 1, "301", 51403397, "Nguyen", "C");
        SinhVienCNTT e = new SinhVienCNTT(10, 2, 2, "301", 51403398, "Nguyen", "D");
        SinhVienCNTT f = new SinhVienCNTT(13, 2, 1, "301", 51403399, "Nguyen", "G");
        SinhVienCNTT g = new SinhVienCNTT(4, 1, 2, "301", 51403355, "Nguyen", "B");
        SinhVienCNTT h = new SinhVienCNTT(16, 1, 2, "301", 51403356, "Nguyen", "C");

        SinhVienToan a1 = new SinhVienToan(9, 1, 2, "302", "51403374", "Nguyen", "A");
        SinhVienToan b1 = new SinhVienToan(12, 2, 1, "302", "51403375", "Nguyen", "G");
        SinhVienToan c1 = new SinhVienToan(14, 1, 1, "302", "51403376", "Nguyen", "F");
        SinhVienToan d1 = new SinhVienToan(11, 2, 1, "302", "51403377", "Nguyen", "C");
        SinhVienToan e1 = new SinhVienToan(5, 2, 2, "302", "51403378", "Nguyen", "C");
        SinhVienToan f1 = new SinhVienToan(6, 2, 1, "302", "51403379", "Nguyen", "G");
        SinhVienToan g1 = new SinhVienToan(7, 1, 2, "302", "51403315", "Nguyen", "B");
        SinhVienToan h1 = new SinhVienToan(8, 1, 2, "302", "51403316", "Nguyen", "C");

        ListSV listSV = new ListSV();
        listSV.setList(new ArrayList<SinhVien>());

        listSV.Add(a);
        listSV.Add(f1);
        listSV.Add(d);
        listSV.Add(h);
        listSV.Add(a1);
        listSV.Add(e);
        listSV.Add(f);
        listSV.Add(g);
        listSV.Add(b1);
        listSV.Add(c1);
        listSV.Add(d1);
        listSV.Add(e1);
        listSV.Add(g1);
        listSV.Add(b);
        listSV.Add(c);
        listSV.Add(h1);

        listSV.arrange();
        listSV.getList().forEach(x -> System.out.println(x));
        System.out.println();
        //listSV.searchSV(new SinhVienCNTT(144, 45, 1, "1566", 12333, "a", "a")).forEach(x -> System.out.println(x));
        listSV.getList().stream().filter(x -> x.getTen().equalsIgnoreCase("c") && x.getTo() == 1).forEach(x -> System.out.println(x));
    }
}
