import java.util.*;

public class ListSV {
    private List<SinhVien> list;

    public ListSV() {
        //this.list = new ArrayList<SinhVien>();
    }

    public ListSV(List<SinhVien> list) {
        this.list = list;
    }

    public List<SinhVien> getList() {
        return list;
    }

    public void setList(List<SinhVien> list) {
        this.list = list;
    }

    // Add(SV): nhap them sinh vien vao danh sach;
    public boolean Add(SinhVien x) {
        if (this.list.stream().noneMatch(y -> y.getMaSoSV().equals(x.getMaSoSV()))) {
            this.list.add(x);
        }
        else {
            return false;
        }
        return true;
    }

    // Add(SV, index): nhap them sinh vien vao vi tri index cua danh sach;
    public boolean Add(SinhVien x, int index) {
        if (index > this.list.size() || index < 0) {
            return false;
        }
        for (SinhVien y : this.list) {
            if (y.getMaSoSV().equals(x.getMaSoSV())) {
                return false;
            }
        }
        this.list.add(index, x);
        return true;
    }

    // Update thong tin sinh vien;
    public void updateSV(int index, int STT, int Nhom, int To, String Lop, Object MaSoSV, String HoLot, String Ten) {
        this.list.get(index).setMaSoSV(MaSoSV);
        this.list.get(index).setLop(Lop);
        this.list.get(index).setTo(To);
        this.list.get(index).setSTT(STT);
        this.list.get(index).setHoLot(HoLot);
        this.list.get(index).setTen(Ten);
        this.list.get(index).setNhom(Nhom);
    }

    // Search(SV): tim sinh vien co thong tin trung voi SV;
    public ArrayList<SinhVien> searchSV(SinhVien x) {
        ArrayList<SinhVien> result = new ArrayList<SinhVien>();
        for (SinhVien y : this.list) {
            if (y.getHoLot().equalsIgnoreCase(x.getHoLot()) || y.getSTT() == x.getSTT() || y.getTo() == x.getTo() ||
                    y.getNhom() == x.getNhom() || y.getLop().equalsIgnoreCase(x.getLop()) ||
                    y.getTen().equalsIgnoreCase(x.getTen()) || y.getMaSoSV().equals(x.getMaSoSV())){
                result.add(y);
            }
        }
        return result;
    }

    // Delete(SV): xoa sinh vien khoi danh sach;
    public boolean Delete(SinhVien x) {
        for (SinhVien y : this.list) {
            if (y.equals(x)) {
                this.list.remove(y);
                return true;
            }
        }
        return false;
    }

    // Delete(index): xoa sinh vien tai vi tri index;
    public boolean Delete(int index) {
        if (index < 0 || index > this.list.size()) {
            return false;
        }
        this.list.remove(index);
        return true;
    }

    //Sap xep lai thu tu sinh vien theo tung thuoc tinh;
    public void arrange() {
        this.list.sort((x1, x2) -> {
            int cmp = x1.getLop().compareTo(x2.getLop());
            if (cmp == 0) {
                cmp = Integer.compare(x1.getTo(), x2.getTo());
            }
            if (cmp == 0) {
                cmp = Integer.compare(x1.getNhom(), x2.getNhom());
            }
            if (cmp == 0) {
                cmp = x1.getTen().compareTo(x2.getTen());
            }
            if (cmp == 0) {
                cmp = Integer.compare(x1.getSTT(), x2.getSTT());
            }
            return cmp;
        });
    }
}
