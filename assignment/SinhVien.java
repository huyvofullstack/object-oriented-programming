public class SinhVien {
    private int STT;
    private int Nhom;
    private int To;
    private Object MaSoSV;
    private String HoLot;
    private String Ten;
    private String Lop;

    public SinhVien() {
        this(0, 0, 0, "", "", "", "");
    }

    public SinhVien(int STT, int Nhom, int To, String Lop, Object MaSoSV, String HoLot, String Ten) {
        this.STT = STT;
        this.Nhom = Nhom;
        this.To = To;
        this.MaSoSV = MaSoSV;
        this.Ten = Ten;
        this.HoLot = HoLot;
        this.Lop = Lop;
    }

    public String getTen() {
        return this.Ten;
    }

    public String getLop() {
        return this.Lop;
    }

    public String getHoLot() {
        return this.HoLot;
    }

    public Object getMaSoSV() {
        return this.MaSoSV;
    }

    public int getSTT() {
        return this.STT;
    }

    public int getTo() {
        return this.To;
    }

    public int getNhom() {
        return this.Nhom;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public void setHoLot(String HoLot) {
        this.HoLot = HoLot;
    }

    public void setMaSoSV(Object MaSoSV) {
        this.MaSoSV = MaSoSV;
    }

    public void setLop(String Lop) {
        this.Lop = Lop;
    }

    public void setSTT(int STT) {
        this.STT = STT;
    }

    public void setTo(int To) {
        this.To = To;
    }

    public void setNhom(int Nhom) {
        this.Nhom = Nhom;
    }

    @Override
    public String toString() {
        return "STT: " + this.STT +  " - " + this.HoLot + " " + this.Ten + " - Nhom: " + this.Nhom + " - To: "
                + this.To + " - Lop: " + this.Lop + " - MSSV: " + this.MaSoSV;
    }
}
