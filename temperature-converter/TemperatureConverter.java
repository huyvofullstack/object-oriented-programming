public class TemperatureConverter {
    public static double CelsiusToFahrenheit(String temperatureCelsius) {
        double celsius = Double.parseDouble(temperatureCelsius);
        double fahrenheit = (celsius * 9 / 5) + 32;
        return Math.rint(fahrenheit);
    }

    public static double FahrenheitToCelsius(String temperatureFahrenheit) {
        double fahrenheit = Double.parseDouble(temperatureFahrenheit);
        double celsius = (fahrenheit - 32) * 5 / 9;
        return Math.rint(celsius);
    }
}
