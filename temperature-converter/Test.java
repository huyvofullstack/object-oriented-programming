import javax.swing.*;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please select the converter direction:");
        System.out.println("1. Celsius to Fahrenheit.");
        System.out.println("2. Fahrenheit to Celsius.");
        System.out.print("-> Enter here: ");
        String option = scanner.next();

        switch (option) {
            case "1":
                double fahrenheit;
                System.out.print("Please enter the Celsius temperature: ");
                String temperatureCelsius = scanner.next();
                fahrenheit = TemperatureConverter.CelsiusToFahrenheit(temperatureCelsius);
                System.out.println("Temperature in Fahrenheit: " + fahrenheit + " F");
                break;
            case "2":
                double celsius;
                System.out.print("Please enter the Fahrenheit temperature: ");
                String temperatureFahrenheit = scanner.next();
                celsius = TemperatureConverter.FahrenheitToCelsius(temperatureFahrenheit);
                System.out.println("Temperature in Celsius: " + celsius + " C");
                break;
            default:
                System.out.println("Error: Missing or Invalid Temperature Values!");
        }
    }
}
