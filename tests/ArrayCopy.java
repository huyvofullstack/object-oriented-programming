public class ArrayCopy {

    public static void main(String[] args) {
        int [] a1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int [] a2 = new int[a1.length];
        System.arraycopy(a1, 0, a2, 0, a1.length);
        for (int i = 0; i < a2.length; i++) {
            System.out.println(a2[i]);
        }
    }
    
}
